package devoir;

import javax.media.opengl.GL2;

/**
 * Created by rgaby on 02/03/15.
 */
public class Pyramide3D implements Displayable3D {
    @Override
    public void display(GL2 gl) {
        dessinerPyramide(gl);
    }

    @Override
    public void init(GL2 gl) {

    }


    private void dessinerPyramide(GL2 gl){
        gl.glBegin(GL2.GL_TRIANGLES);
        gl.glColor3f(0.0f,1.0f,1.0f);
        gl.glVertex3f( 0.0f, 1.0f, 0.0f);
        gl.glVertex3f(-1.0f,-1.0f, 1.0f);
        gl.glVertex3f( 1.0f,-1.0f, 1.0f);
        gl.glColor3f(1.0f,1.0f,0.0f);
        gl.glVertex3f( 0.0f, 1.0f, 0.0f);
        gl.glVertex3f( 1.0f,-1.0f, 1.0f);
        gl.glVertex3f( 1.0f,-1.0f, -1.0f);
        gl.glColor3f(0.0f,1.0f,0.0f);
        gl.glVertex3f( 0.0f, 1.0f, 0.0f);
        gl.glVertex3f( 1.0f,-1.0f, -1.0f);
        gl.glVertex3f(-1.0f,-1.0f, -1.0f);
        gl.glColor3f(1.0f,0.0f,1.0f);
        gl.glVertex3f( 0.0f, 1.0f, 0.0f);
        gl.glVertex3f(-1.0f,-1.0f,-1.0f);
        gl.glVertex3f(-1.0f,-1.0f, 1.0f);
        gl.glEnd();
    }

}
