package devoir;

import javax.media.opengl.GL2;

/**
 * Created by rgaby on 02/03/15.
 */
public interface Displayable3D {
    public void display(GL2 gl);
    public void init(GL2 gl);
}
