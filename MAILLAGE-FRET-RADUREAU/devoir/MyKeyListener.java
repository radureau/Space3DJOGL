package devoir;

/**
 * Created by rgaby on 21/02/15.
 */
import javax.media.opengl.awt.GLCanvas;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyKeyListener implements KeyListener{
    GLCanvas canvas;
    Vision3D vision3D;
    public MyKeyListener(GLCanvas canvas, Vision3D vision3D){
        this.canvas = canvas;
        this.vision3D = vision3D;
    }

    public void keyPressed(KeyEvent e){
        int keyCode = e.getKeyCode();
        if(vision3D.cameraMode){
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    System.out.println("Aller tout droit");
                    this.vision3D.camera.moveForward(1.f);
                    break;
                case KeyEvent.VK_DOWN:
                    System.out.println("Reculer");
                    this.vision3D.camera.moveBehind(1.f);
                    break;
                case KeyEvent.VK_LEFT:
                    System.out.println("Aller à gauche");
                    this.vision3D.camera.moveLeft(1.f);
                    break;
                case KeyEvent.VK_RIGHT:
                    System.out.println("Aller à droite");
                    this.vision3D.camera.moveRight(1.f);
                    break;
                default:
                    key(e.getKeyChar(), e.getKeyLocation(), e.getKeyLocation());
            }
        }
        else if(vision3D.translationMode) {
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    this.vision3D.tz += .1f;
                    break;
                case KeyEvent.VK_DOWN:
                    this.vision3D.tz -= .1f;
                    break;
                case KeyEvent.VK_LEFT:
                    this.vision3D.tx += .1f;
                    break;
                case KeyEvent.VK_RIGHT:
                    this.vision3D.tx -= .1f;
                    break;
                default:
                    key(e.getKeyChar(), e.getKeyLocation(), e.getKeyLocation());
            }
        }
        else {
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    this.vision3D.incrAngleX++;
                    break;
                case KeyEvent.VK_DOWN:
                    this.vision3D.incrAngleX--;
                    break;
                case KeyEvent.VK_LEFT:
                    this.vision3D.incrAngleZ++;
                    break;
                case KeyEvent.VK_RIGHT:
                    this.vision3D.incrAngleZ--;
                    break;
                default:
                    key(e.getKeyChar(), e.getKeyLocation(), e.getKeyLocation());
            }
        }
    }
    public void keyReleased(KeyEvent e){}
    public void keyTyped(KeyEvent e){}

    void key(char key, int x, int y){
        switch (key) {
            case 'o':
                if(vision3D.cameraMode) {
                    System.out.println("CAMERA Rotation HAUT");
                    this.vision3D.camera.moveUp(1.f);
                }
                else if( vision3D.translationMode)
                    this.vision3D.ty -= .1f;
                else
					++this.vision3D.incrAngleY;
                break;
            case 'l':
                if(vision3D.cameraMode) {
                    System.out.println("CAMERA Rotation BAS");
                    this.vision3D.camera.moveDown(1.f);
                }
                else if( vision3D.translationMode)
                    this.vision3D.ty += .1f;
                else
					--this.vision3D.incrAngleY;
                break;
            case '1':
                vision3D.switchToFix();
                vision3D.camera = new Camera();
                System.out.println("\tDEVANT");
                break;
            case '2':
                vision3D.switchToFix();
                this.vision3D.incrAngleX=180;
                this.vision3D.angleX=1;
                System.out.println("\tÀ L'ENVERS");
                break;
            case '3':
                vision3D.switchToFix();
                this.vision3D.incrAngleY=180;
                this.vision3D.angleY=1;
                System.out.println("\tDERRIERE");
                break;
            case '5':
                if(vision3D.cameraMode)
					vision3D.cameraMode = false;
				else{
					vision3D.animatedRotation = false;
					vision3D.cameraMode = true;
				}
                System.out.println("\tMODE CAMERA "+(vision3D.cameraMode?"ON":"OFF"));
                break;
            default:
                System.out.println("\t"+key+"\tpressed");
        }
    }

}
