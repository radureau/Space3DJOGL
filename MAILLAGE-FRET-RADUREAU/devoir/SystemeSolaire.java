package devoir;

import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.util.gl2.GLUT;
import java.nio.*;
import com.jogamp.common.nio.Buffers;
import com.jogamp.common.nio.PointerBuffer;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import java.io.InputStream;
import java.io.IOException;



import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants

/**
 * Created by rgaby on 14/03/15.
 */
public class SystemeSolaire implements  Displayable3D {

    Planete soleil;
    Planete mercure;
    Planete venus;
    Planete terre;
    Planete mars;
    Planete jupiter;
    Planete saturne;
    Planete uranus;
    Planete neptune;

    Planete lesEtoiles;

    public SystemeSolaire(){
        soleil = new Planete(3,
                0,0,0,
                0,0,0,
                0,0,0,
                0,0,
                "./../Textures/sun01.jpg");
            mercure = new Planete(0.5f,
                    4.6f,0,0,
                    0,1,0,
                    0,1,1,
                    0.5f,0.5f,
                    "./../Textures/planet01.jpg");
                mercure.autoVitesseRotation();
            venus = new Planete(0.9f,
                    7.2f,0,0,
                    0,1,0,
                    0,1,1,
                    0.5f,0.5f,
                    "./../Textures/planet12.jpg");
                venus.autoVitesseRotation();
            terre = new Planete(1,
                    10,0,0,
                    0,1,0,
                    0,1,1,
                    0.5f,0.5f,
                    "./../Textures/planet11.jpg");
                terre.ajouterLune();
                terre.ajouterSatellite(new SatelliteIcosaedrique());
                terre.autoVitesseRotation();
            mars = new Planete(0.6f,
                    16.6f,0,0,
                    0,1,0,
                    0,1,1,
                    0.5f,0.5f,
                    "./../Textures/planet10.png");
                mars.autoVitesseRotation();
            jupiter = new Planete(8f,
                54.4f,0,0,
                0,1,0,
                0,1,1,
                0.5f,0.5f,
                "./../Textures/planet06.png");
                jupiter.autoVitesseRotation();
            saturne = new Planete(4,
                    100,0,0,
                    0, 1, 0,
                    0,1,0,
                    0.2f,0.2f,
                    null,
                    true, "./../Textures/planet07.png");
                saturne.autoVitesseRotation();
            uranus = new Planete(2,
                190,0,0,
                0, 1, 0,
                0,1,0,
                0.2f,0.2f,
                null,
                true, "./../Textures/planet07.png");
                uranus.autoVitesseRotation();
        this.soleil.ajouterSatellite(mercure);
        this.soleil.ajouterSatellite(venus);
        this.soleil.ajouterSatellite(terre);
        this.soleil.ajouterSatellite(mars);
        this.soleil.ajouterSatellite(saturne);
        this.soleil.ajouterSatellite(uranus);

        this.soleil.brille = true;

        //étoiles
        lesEtoiles = new Planete(400,
                0,0,0,
                0,0,0,
                0,1,0,
                0,0.01f,
                "./../Textures/etoiles.jpg"
                );
    }

    @Override
    public void display(GL2 gl) {
        gl.glShadeModel(GL2.GL_SMOOTH); //Goureaud
        soleil.display(gl);
        gl.glCullFace(GL_FRONT);
        gl.glRotated(90,1,0,0);
        lesEtoiles.display(gl);
        gl.glCullFace(GL_BACK);
    }

    @Override
    public void init(GL2 gl) {
        soleil.init(gl);
        lesEtoiles.init(gl);
    }

}

class Planete implements  Displayable3D {

    //attributs
    protected float taille;
    protected float distance_x, distance_y, distance_z;//distance par rapport à la planete parent (par défaut le soleil)
    protected double rotation_x, rotation_y, rotation_z;//rotation autour de la planete parent (par défaut le soleil)
    protected double self_rotation_x, self_rotation_y, self_rotation_z;//rotation de la planete sur elle meme
    protected float vitesse_rotation, vitesse_self_rotation;
    protected String uriTexture;
    protected Texture texture;
    protected Collection<Planete> satellites;
    protected boolean aUnAnneau;
    protected String uriTextureAnneau;
    protected Texture textureAnneau;

    boolean brille;

    //compteurs
    protected double rotation=0, self_rotation=0;

    //outils
    protected GLUT glut = new GLUT();
    protected GLU glu = new GLU();

    public Planete(float taille,
                   float distance_x, float distance_y, float distance_z,
                   double rotation_x, double rotation_y, double rotation_z,
                   double self_rotation_x, double self_rotation_y, double self_rotation_z,
                   float vitesse_rotation, float vitesse_self_rotation,
                   String uriTexture){
        this.aUnAnneau = false;

        this.taille = taille;
        this.distance_x = distance_x;this.distance_y = distance_y;this.distance_z = distance_z;
        this.rotation_x = rotation_x;this.rotation_y = rotation_y;this.rotation_z = rotation_z;
        this.self_rotation_x = self_rotation_x;this.self_rotation_y = self_rotation_y;this.self_rotation_z = self_rotation_z;
        this.vitesse_rotation = vitesse_rotation;this.vitesse_self_rotation = vitesse_self_rotation;
        this.uriTexture = uriTexture;

        this.satellites = new ArrayList<Planete>();

    }
    public Planete(float taille,
                   float distance_x, float distance_y, float distance_z,
                   double rotation_x, double rotation_y, double rotation_z,
                   double self_rotation_x, double self_rotation_y, double self_rotation_z,
                   float vitesse_rotation, float vitesse_self_rotation,
                   String uriTexture,
                   boolean aUnAnneau,
                   String uriTextureAnneau) {
        this(taille,
                distance_x, distance_y, distance_z,
                rotation_x, rotation_y, rotation_z,
                self_rotation_x, self_rotation_y, self_rotation_z,
                vitesse_rotation, vitesse_self_rotation,
                uriTexture);

        this.aUnAnneau = aUnAnneau;
        this.uriTextureAnneau = uriTextureAnneau;

    }

    @Override
    public void init(GL2 gl) {
        if(brille){
            gl.glEnable(GL_LIGHT6);
            gl.glLightfv(GL_LIGHT6, GL_DIFFUSE, new float[]{1.0f,1.0f,1.0f, 1.0f}, 0);//on active l'éclairage
            gl.glLightfv(GL_LIGHT6, GL_AMBIENT, new float[]{1.0f,1.0f,1.0f,1.0f}, 0);
            gl.glLightfv(GL_LIGHT6,GL_SPECULAR,new float[]{ 1f,1f,1f,1f }, 1);
        }
        if(uriTexture != null)
            try {
                //~ texture = TextureIO.newTexture(new File(uriTexture), false);
                
                int l = uriTexture.length();
                InputStream stream = getClass().getResourceAsStream(uriTexture);
                TextureData data=TextureIO.newTextureData(gl.getGLProfile(),stream,false,uriTexture.substring(l-3,l));
                texture = TextureIO.newTexture(data);
                
                if(aUnAnneau && uriTextureAnneau!=null) {
                    textureAnneau = TextureIO.newTexture(new File(uriTextureAnneau), false);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        for(Planete p : satellites)
            p.init(gl);
    }

    @Override
    public void display(GL2 gl) {
        gl.glPushMatrix();

        //Rotation autour de la planète parent
        rotation += vitesse_rotation;
        gl.glRotated(rotation, rotation_x, rotation_y, rotation_z);

        //Décalage par rapport à la planète parent
        gl.glTranslatef(distance_x, distance_y, distance_z);

        //Dessin des satellites
        for(Planete planete : satellites)
            planete.display(gl);

        //Rotation sur soi
        self_rotation += vitesse_self_rotation;
        gl.glRotated(self_rotation, self_rotation_x, self_rotation_y, self_rotation_z);

        if(brille){
            // Set material properties.
            float[] rgba = {1f, 1f, 1f};
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_EMISSION, rgba, 0);
            gl.glLightfv(GL_LIGHT6,GL_POSITION,new float[]{ 0.0f,0.0f,0.0f,1.0f }, 0);
        }
        else{
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT, new float[]{ 0,0,0,0f }, 0);
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, new float[]{ 0.8f,0.8f,0.8f,1f }, 0);
            gl.glMateriali(GL.GL_FRONT_AND_BACK, gl.GL_SPECULAR, 0);
        }

        if(texture!=null) {
            //Application de la texture
            texture.enable(gl);
            texture.bind(gl);

            //Dessin de la planete
            GLUquadric globe = glu.gluNewQuadric();
            glu.gluQuadricTexture(globe, true);
            glu.gluQuadricDrawStyle(globe, GLU.GLU_FILL);
            glu.gluQuadricNormals(globe, GLU.GLU_FLAT);
            glu.gluQuadricOrientation(globe, GLU.GLU_OUTSIDE);
            final float radius = taille;
            final int slices = 46;
            final int stacks = 46;

            if(brille) {
                gl.glPushMatrix();
                gl.glRotated(Math.random(), Math.random(), Math.random(), Math.random());
                glu.gluSphere(globe, radius-Math.random()/100, slices, stacks);
                gl.glPopMatrix();
            }
            else{
                glu.gluSphere(globe, radius, slices, stacks);
            }
            glu.gluDeleteQuadric(globe);
        }else{
            //Application de la couleur
            gl.glColor3f(1, 1, 1);
            //Dessin de la planete
            glut.glutSolidSphere(taille,32,32);
        }

        //dessin anneau
        if(aUnAnneau){
            if(textureAnneau!=null){
                textureAnneau.enable(gl);
                textureAnneau.bind(gl);
            }
            else if(texture!=null){
                texture.enable(gl);
                texture.bind(gl);
            }
            else{
                gl.glColor3f(0, 1, 0);
            }
            glut.glutSolidTorus(0.1, taille*4/3, 2, 50);
        }

        if(brille) {
            // Unset material properties.

            float[] mat = {0f, 0f, 0f};
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_EMISSION, mat, 0);
            gl.glLightfv(GL_LIGHT6,GL_SPECULAR,new float[]{ 0,0,0,0 }, 0);

        }
        else{
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT, new float[]{ 0.2f,0.2f,0.2f,1f }, 0);
            gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, new float[]{ 0.8f,0.8f,0.8f,1f }, 0);
            gl.glMateriali(GL.GL_FRONT_AND_BACK, gl.GL_SPECULAR, 0);
        }

            gl.glPopMatrix();
    }

    public void ajouterSatellite(Planete planete){
        this.satellites.add(planete);
    }

    public void ajouterLune(){
        Planete lune = new Planete(this.taille/3,
                this.taille*2, 0, 0,
                -this.rotation_x,-this.rotation_y,-this.rotation_z,
                -this.self_rotation_x,-this.self_rotation_y,-this.self_rotation_z,
                this.vitesse_rotation*12.4f, this.vitesse_self_rotation*3,
                "./../Textures/moon01.jpg");
        this.satellites.add(lune);
    }

    public void autoVitesseRotation(){
        double R = Math.sqrt(distance_x*distance_x + distance_y*distance_y + distance_z*distance_z);
        double T = Math.sqrt(R*R*R);
        this.vitesse_rotation = (float)T/360;
    }

}

class SatelliteIcosaedrique extends Planete{

    public SatelliteIcosaedrique(){
        super(0.3f,
                1.5f, 0f, 0f,
                0f,0f,1f,
                -1f,0f,0f,
                1f,1f,
                null);
    }

    @Override
    public void display(GL2 gl) {
        gl.glPushMatrix();

        //Rotation autour de la planète parent
        rotation += vitesse_rotation;
        gl.glRotated(rotation, rotation_x, rotation_y, rotation_z);

        //Décalage par rapport à la planète parent
        gl.glTranslatef(distance_x, distance_y, distance_z);

        //Dessin des satellites
        for(Planete planete : satellites)
            planete.display(gl);

        //Rotation sur soi
        self_rotation += vitesse_self_rotation;
        gl.glRotated(self_rotation, self_rotation_x, self_rotation_y, self_rotation_z);

        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT, new float[]{ 0,0,0,0f }, 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, new float[]{ 0.8f,0.8f,0.8f,1f }, 0);
        gl.glMateriali(GL.GL_FRONT_AND_BACK, gl.GL_SPECULAR, 0);

        gl.glScaled(0.2, 0.2, 0.2);
        glut.glutSolidIcosahedron();

        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT, new float[]{ 0.2f,0.2f,0.2f,1f }, 0);
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, new float[]{ 0.8f,0.8f,0.8f,1f }, 0);
        gl.glMateriali(GL.GL_FRONT_AND_BACK, gl.GL_SPECULAR, 0);

        gl.glPopMatrix();
    }
}
