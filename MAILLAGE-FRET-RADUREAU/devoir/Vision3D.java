package devoir;

/**
 * Created by rgaby on 21/02/15.
 */
import javax.media.opengl.*;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.glu.GLU;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.FPSAnimator;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants


public class Vision3D implements GLEventListener{

    FPSAnimator anim;
    List<Displayable3D> objets3D;
    Camera camera;

    public Vision3D(FPSAnimator anim){
        this.anim = anim;
        this.objets3D = new ArrayList<Displayable3D>();
        camera = new Camera();
    }

    private GLU glu = new GLU();
    private GLUT glut = new GLUT();

    //Translations
    float tx, ty, tz = 0f;

    //Rotations
    double angleX=0d, incrAngleX=0d;
    double angleY=0d, incrAngleY=0d;
    double angleZ=0d, incrAngleZ=0d;
    float axeX=1f, axeY=1f, axeZ=1f;

    //Modes
    boolean animatedRotation = false;
    boolean translationMode = false;
    boolean cameraMode = false;


    /*
    DISPLAY
     */

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClear( gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT );

        gl.glLoadIdentity();//on replace la matrice à l'origine
        glu.gluLookAt(camera.position[0],camera.position[1],camera.position[2],
                camera.position[0]+camera.direction[0], camera.position[1]+camera.direction[1], camera.position[2]+camera.direction[2],
                camera.up[0], camera.up[1], camera.up[0]);

        gl.glTranslated(0,40,-40.0);//on place la matrice
        gl.glTranslated(tx,ty,tz);//on bouge tout le monde sauf la camera

        if(animatedRotation) {
            angleX+=incrAngleX;
            angleY+=incrAngleY;
            angleZ+=incrAngleZ;
        }

        gl.glRotated(angleX+incrAngleX, axeX, 0f, 0f);
        gl.glRotated(angleY+incrAngleY, 0f, axeY, 0f);
        gl.glRotated(angleZ+incrAngleZ, 0f, 0f, axeZ);
        
        for(Displayable3D d3d : objets3D){
			gl.glPushMatrix();
            d3d.display(gl);
            gl.glPopMatrix();
        }

    }

    /*
    FIN DISPLAY
     */

    public void repaint(GLAutoDrawable drawable){
        this.display(drawable);
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {

    }

    @Override
    public void init(GLAutoDrawable drawable) {
        // TODO Auto-generated method stub
        GL2 gl = drawable.getGL().getGL2();
        drawable.setGL(new DebugGL2(gl));

        gl.glDepthFunc(GL2.GL_LEQUAL);
        gl.glShadeModel(GL2.GL_SMOOTH); //Goureaud
        //gl.glShadeModel(GL2.GL_FLAT); // facettes
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

        gl.glClearColor (0f, 0f, 0f, 1.0f);
        gl.glEnable(gl.GL_DEPTH_TEST);
        
		drawable.getAnimator().setUpdateFPSFrames(10, null);
		drawable.getGL().setSwapInterval(0);

        gl.glLightModelfv(GL_LIGHT_MODEL_AMBIENT, new float[]{0f,0f,0f,0f}, 1);
        gl.glEnable(GL_LIGHTING);

        gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2.GL_LESS);
        gl.glEnable(GL_NORMALIZE);
            

        for(Displayable3D d3d : objets3D)
            d3d.init(gl);
        gl.glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        gl.glEnable( GL_CULL_FACE ) ; gl.glCullFace( GL_BACK );

    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glViewport(0, 0, (int) width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        glu.gluPerspective(45.0,  width/(float) height, 0.01, 5000.0);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
    }

    public void switchAnimatedMode(){
        animatedRotation = ! animatedRotation;
        if(animatedRotation){
            angleX += incrAngleX;
            angleY += incrAngleY;
            angleZ += incrAngleZ;
            incrAngleX = 0;
            incrAngleY = 0;
            incrAngleZ = 0;
        }
    }
    public void switchToFix(){
        animatedRotation=false;
        incrAngleX = 0;
        incrAngleY = 0;
        incrAngleZ = 0;
        angleX = 0;
        angleY = 0;
        angleZ = 0;
    }

    public void add(final Displayable3D d3d){
        this.objets3D.add(d3d);
        (new Thread(new Runnable() {
            @Override
            public void run() {
                GL2 gl = null;
                while(gl==null){
                    try{
                        gl = GLU.getCurrentGL().getGL2();
                    }
                    catch (Exception e){}
                }
                d3d.init(gl);
            }
        })).start();
    }
}
