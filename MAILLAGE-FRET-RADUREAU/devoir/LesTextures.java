package devoir;

import com.jogamp.opengl.util.texture.*;

import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import javax.media.opengl.GL2;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.io.*;

import static javax.media.opengl.fixedfunc.GLLightingFunc.*;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_LIGHT0;
import static javax.media.opengl.fixedfunc.GLLightingFunc.GL_SPECULAR;

/**
 * Created by rgaby on 14/03/15.
 */
public class LesTextures implements  Displayable3D {
	private GLUT glut = new GLUT();
	private GLU glu = new GLU();
    Texture texture;

	public LesTextures ()
	{
        texture=null;
	}

    float c=0;

	@Override
    public void display(GL2 gl) {
		texture.enable(gl);
		texture.bind(gl);
        gl.glColor3f(1f, 1f, 1f);
        GLUquadric earth = glu.gluNewQuadric();
        glu.gluQuadricTexture(earth, true);
        glu.gluQuadricDrawStyle(earth, GLU.GLU_FILL);
        glu.gluQuadricNormals(earth, GLU.GLU_FLAT);
        glu.gluQuadricOrientation(earth, GLU.GLU_OUTSIDE);
        final float radius = 1;
        final int slices = 32;
        final int stacks = 32;
        glu.gluSphere(earth, radius, slices, stacks);
        glu.gluDeleteQuadric(earth);

        texture.disable(gl);
		
		gl.glColor3f(1f, 1f, 1f);

        gl.glPushMatrix();

        glut.glutSolidTeapot(1);

        gl.glRotated(c++, 0, 0, 1);
        gl.glTranslated(3,-1,0);

        gl.glEnable(GL_LIGHT6);
        gl.glLightfv(GL_LIGHT6, GL_DIFFUSE, new float[]{1.0f,1.0f,1.0f, 1.0f}, 0);//on supprime l'éclairage général par défaut
        gl.glLightfv(GL_LIGHT6, GL_AMBIENT, new float[]{1.0f,0.0f,0.0f,1.0f}, 0);
        gl.glLightfv(GL_LIGHT6,GL_POSITION,new float[]{ 0.0f,0.0f,0.0f,1.0f }, 0);
        gl.glLightfv(GL_LIGHT6,GL_SPECULAR,new float[]{ 1.0f,1.0f,1.0f,1.0f }, 0);
        //gl.glDisable(GL_LIGHT6);

        glut.glutSolidTeapot(0.3);/*
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_EMISSION, new float[]{0.0f, 0.0f, 0.0f, 0.0f}, 0);
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK,gl.GL_AMBIENT_AND_DIFFUSE, new float[]{0.0f,0.0f,0.0f,0.0f}, 0);
*/
        gl.glPopMatrix();


    }

    @Override
    public void init(GL2 gl) {
        try {
            texture = TextureIO.newTexture(new File("/home/rgaby/IdeaProjects/untitled/src/Textures/sun01.jpg"), false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
