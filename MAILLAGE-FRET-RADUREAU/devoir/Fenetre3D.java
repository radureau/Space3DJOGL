package devoir;

/**
 * Created by rgaby on 21/02/15.
 */
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

import com.jogamp.opengl.util.FPSAnimator;

import java.awt.*;

public class Fenetre3D extends JFrame{
    Vision3D vision3D;

    private static final long serialVersionUID = 1L;
    public Fenetre3D(){
        setTitle("Fenetre3D");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 400);

        final GLCanvas canvas = new GLCanvas ();
        final FPSAnimator anim = new FPSAnimator(canvas,30);
        vision3D = new Vision3D(anim);

        canvas.addGLEventListener(vision3D);
        this.getContentPane().add(canvas);

        final MyKeyListener myKeyListener=new MyKeyListener(canvas, vision3D);
        canvas.addKeyListener(myKeyListener);

        final MyMouseListener myMouseListener=new MyMouseListener(canvas, vision3D);
        canvas.addMouseListener(myMouseListener);
        canvas.addMouseMotionListener(myMouseListener);

        this.setUndecorated(true);  // no decoration such as title and scroll bars
        this.setExtendedState(Frame.MAXIMIZED_BOTH);  // full screen mode

        anim.start();
        setVisible(true);
    }

    public void add(Displayable3D d3d){
        this.vision3D.add(d3d);
    }
}
