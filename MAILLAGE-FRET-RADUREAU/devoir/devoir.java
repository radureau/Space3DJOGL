package devoir;

public class devoir {

    public static void main(String[] args) {
        Fenetre3D f3d = new Fenetre3D();

        //f3d.add(new LesTextures());
        f3d.add(new SystemeSolaire());
        //f3d.add(new Maison3D());
        //f3d.add(new Pyramide3D());

        SpaceShip sp = new SpaceShip("./../3DModels/StarDestroyer/StarDestroyer.json");
        //~ SpaceShip sp2 = new SpaceShip("./../3DModels/X-Wing/X-Wing.json", 1f);
        //~ SpaceShip sp3 = new SpaceShip("./../3DModels/TieInterceptor/TieInterceptor.json", 0.1f);
        //~ SpaceShip sp4 = new SpaceShip("./../3DModels/Venator/Venator.json", 0.1f);
        f3d.add(sp);
        //~ f3d.add(sp2);
        //~ f3d.add(sp3);
        //~ f3d.add(sp4);
    }
}
