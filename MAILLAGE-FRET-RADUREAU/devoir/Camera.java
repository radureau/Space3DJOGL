package devoir;

public class Camera{
	// pour le glu.lookAt on regarde à position + direction
	float position[], direction[], up[];
	float theta, phi;
	
	
	public Camera(){
		position = new float[3];	position[0]=0; position[1]=0; position[2]=0;
		direction = new float[3];	direction[0]=0; direction[1]=0; direction[2]=-1;
		up = new float[3];			up[0]=0; up[1]=1; up[2]=0;
		theta=3.4f; phi=0;
	} 
	
	float[] cross(float[] v1, float[] v2){
		float res[] = new float[3];
		res[0] = v1[1] * v2[2]  -  v1[2] * v2[1];
		res[1] = v1[2] * v2[0]  -  v1[0] * v2[2];
		res[2] = v1[0] * v2[1]  -  v1[1] * v2[0];
		return res;
	}
	float[] normalize(float[] v){
		float res[] = new float[3];
		double longueur = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
		res[0] = (float)( v[0]/longueur );
		res[1] = (float)( v[1]/longueur );
		res[2] = (float)( v[2]/longueur );
		return res;
	}
	
	public void moveLeft(float pas){
		float left[] = normalize(cross(up, direction));
		position[0]+= pas * left[0];
		position[1]+= pas * left[1];
		position[2]+= pas * left[2];
	}
		
	public void moveRight(float pas){
		float left[] = normalize(cross(up, direction));
		position[0]-= pas * left[0];
		position[1]-= pas * left[1];
		position[2]-= pas * left[2];
	}
	public void moveUp(float pas){
		position[0]+= pas * up[0];
		position[1]+= pas * up[1];
		position[2]+= pas * up[2];
	}
	public void moveDown(float pas){
		position[0]-= pas * up[0];
		position[1]-= pas * up[1];
		position[2]-= pas * up[2];
	}
	public void moveForward(float pas){
		position[0]+= pas * direction[0];
		position[1]+= pas * direction[1];
		position[2]+= pas * direction[2];
	}
	public void moveBehind(float pas){
		position[0]-= pas * direction[0];
		position[1]-= pas * direction[1];
		position[2]-= pas * direction[2];
	}
	public void yaw(float angle){//horizontal
		theta+=angle;
		direction[0] = (float)( Math.cos(phi) * Math.sin(theta) );//(float) Math.sin(theta);
		direction[1] = (float) Math.sin(phi); //0;
		direction[2] = (float)( Math.cos(phi) * Math.cos(theta)); //(float) Math.cos(theta);
	}
	public void pitch(float angle){
		phi+=angle;
		phi+=angle;
		if(phi>Math.PI/2-0.001f)
			phi=(float)Math.PI/2-0.001f;
		else if(phi<-Math.PI/2+0.001f)
			phi=(float)-Math.PI/2+0.001f;
		direction[0] = (float)( Math.cos(phi) * Math.sin(theta) );//(float) Math.sin(theta);
		direction[1] = (float) Math.sin(phi); //0;
		direction[2] = (float)( Math.cos(phi) * Math.cos(theta)); //(float) Math.cos(theta);
	}
	
}
