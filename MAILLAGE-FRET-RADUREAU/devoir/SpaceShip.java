package devoir;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.json.*;
import java.io.*;
import java.util.Iterator;
import java.util.Scanner;

import java.nio.*;
import com.jogamp.common.nio.Buffers;
import com.jogamp.common.nio.PointerBuffer;

import static javax.media.opengl.GL2ES1.GL_LIGHT_MODEL_AMBIENT;
import static javax.media.opengl.fixedfunc.GLLightingFunc.*;


/**
 * Created by rgaby on 28/03/15.
 */
public class SpaceShip implements Displayable3D {


    static int nbInstance = 0;
    int idInstance;
    private String uriJson;

    private int VBOpoints;
    private int VBOindices;
    private int VBOnormales;
    
    private float scale;

    public SpaceShip(String uriJson){
        this(uriJson, 1);
    }
    public SpaceShip(String uriJson, float scale){
        this.uriJson = uriJson;
        idInstance = nbInstance++;
        this.scale = scale;
    }

    int nbIndex;

    float c=7*idInstance;
    @Override
    public void display(GL2 gl) {
        gl.glShadeModel(GL2.GL_FLAT); // facettes
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT_AND_DIFFUSE, new float[]{1f, 1f, 1f, 1}, 0);

        c+=0.1f;
        if(c>100+7*idInstance)
            c=0;

        gl.glPushMatrix();

        gl.glTranslated(-50+c, 20, -10);
        gl.glTranslated(0, 0, -20*idInstance);
        

        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOpoints);
        gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, 0);

        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOnormales);
        gl.glNormalPointer(GL2.GL_DOUBLE, 0, 0);
        

        gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, VBOindices);
        for(int i=0; i< 5; ++i) {
            for (int j = 0; j < 3; ++j) {
                gl.glTranslated(0,-4,0);
                gl.glPushMatrix();
                gl.glRotated(90,0,1,0);
                gl.glScaled(scale, scale, scale);
                gl.glDrawElements(GL.GL_TRIANGLES, nbIndex, GL2.GL_UNSIGNED_INT, 0);
                gl.glPopMatrix();
            }
            gl.glTranslated(0,3,0);
            gl.glTranslated(10,0,0);
        }

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
        
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, gl.GL_AMBIENT, new float[]{0f, 0f, 0f}, 0);

        gl.glPopMatrix();
    }

    @Override
    public void init(GL2 gl){

        //Lecture fichier Json
        String jsonString = "";
        try {
            jsonString = new Scanner(new File(uriJson)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Extraction Objet Json
        try {
            JSONObject object = new JSONObject(jsonString);

            JSONArray vertices = object.getJSONArray("vertices").getJSONObject(0).getJSONArray("values");
            JSONArray normals = object.getJSONArray("vertices").getJSONObject(1).getJSONArray("values");
            JSONArray index = object.getJSONArray("connectivity").getJSONObject(0).getJSONArray("indices");
            
            double[] v = new double[vertices.length()];
            for (int i = 0; i < vertices.length(); i++)
			{
				v[i] = vertices.getDouble(i);
			}
            
            double[] n = new double[normals.length()];
            for (int i = 0; i < normals.length(); i++)
			{
				n[i] = vertices.getDouble(i);
			}
            
            int[] i = new int[index.length()];
            for (int j = 0; j < index.length(); j++)
			{
				i[j] = index.getInt(j);
			}

            DoubleBuffer sommets = Buffers.newDirectDoubleBuffer(v);
            DoubleBuffer normales = Buffers.newDirectDoubleBuffer(n);
            IntBuffer indexes = Buffers.newDirectIntBuffer(i);

			int []temp = new int[3];
			gl.glGenBuffers(3, temp, 0);
            VBOpoints = temp[0];
            VBOindices = temp[1];
            VBOnormales = temp[2];

			gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOpoints);
			gl.glBufferData(GL2.GL_ARRAY_BUFFER, sommets.capacity() * Buffers.SIZEOF_DOUBLE, sommets, GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

            nbIndex = i.length;
			gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, VBOindices);
			gl.glBufferData(GL2.GL_ELEMENT_ARRAY_BUFFER, indexes.capacity() * Buffers.SIZEOF_INT, indexes, GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, 0);


            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, VBOnormales);
            gl.glBufferData(GL2.GL_ARRAY_BUFFER, normales.capacity() * Buffers.SIZEOF_DOUBLE, normales, GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

        } catch (JSONException e) {
            System.out.println("Erreur Json dans le SpaceShip.init()");
            System.out.println(e.getMessage());
        }

    }
}
