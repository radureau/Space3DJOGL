package devoir;

import javax.media.opengl.GL2;

import java.nio.*;

import com.jogamp.common.nio.Buffers;

import com.jogamp.common.nio.PointerBuffer;

/**
 * Created by rgaby on 02/03/15.
 */
public class Maison3D implements  Displayable3D {

    @Override
    public void display(GL2 gl) {
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, vertexFB);
		
		//~ gl.glDrawElements(GL2.GL_TRIANGLES, pignons.length, GL2.GL_UNSIGNED_INT,pignonsBuf);
		//~ gl.glDrawElements(GL2.GL_TRIANGLES, toit.length, GL2.GL_UNSIGNED_INT,toitBuf);
		//~ gl.glDrawElements(GL2.GL_TRIANGLES, murs.length, GL2.GL_UNSIGNED_INT,mursBuf);
		
		gl.glMultiDrawElements(GL2.GL_TRIANGLES, vcount,GL2.GL_UNSIGNED_INT, indices, 3);
		
    }

    @Override
    public void init(GL2 gl) {

    }

    static double sommets[]={
            0, 0, 0,
            2, 0, 0,
            2, 1, 0,
            1, 2, 0,
            0, 1, 0,
            0, 0, 2,
            2, 0, 2,
            2, 1, 2,
            1, 2, 2,
            0, 1, 2};

    static int pignons[]={4,3,2,
            4,2,1,
            4,1,0,
            6,7,8,
            6,8,9,
            6,9,5};

    static int toit[]={9,8,3,
            9,3,4,
            8,7,2,
            8,2,3};

    static int murs[]={5,9,4,
            5,4,0,
            1,2,7,
            1,7,6,
            0,1,6,
            0,6,5};
     
     static int count[] = {pignons.length, toit.length, murs.length};
     
     static DoubleBuffer vertexFB =Buffers.newDirectDoubleBuffer(sommets);
     static IntBuffer pignonsBuf = Buffers.newDirectIntBuffer(pignons);
     static IntBuffer toitBuf = Buffers.newDirectIntBuffer(toit);
     static IntBuffer mursBuf = Buffers.newDirectIntBuffer(murs);
     
     static IntBuffer vcount = Buffers.newDirectIntBuffer(count);
     
     static PointerBuffer indices =
		PointerBuffer.allocateDirect(3);
		{
			indices.referenceBuffer(pignonsBuf);
			indices.referenceBuffer(toitBuf);
			indices.referenceBuffer(mursBuf);
			indices.rewind();
		}
}
