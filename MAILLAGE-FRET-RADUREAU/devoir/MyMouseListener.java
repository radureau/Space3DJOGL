package devoir;

/**
 * Created by rgaby on 21/02/15.
 */
import javax.media.opengl.awt.GLCanvas;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MyMouseListener  implements MouseListener, MouseMotionListener{
    GLCanvas canvas;
    Vision3D vision3D;
    int mouseX;
    int mouseY;
    public MyMouseListener(GLCanvas canvas, Vision3D vision3D){
        this.canvas = canvas;
        this.vision3D = vision3D;
    }

    public void mouseClicked(MouseEvent e){
        vision3D.switchAnimatedMode();
    }
    public void mouseEntered(MouseEvent e){
        vision3D.translationMode = false;
        mouseX = e.getX();
        mouseY = e.getY();
    }
    public void mouseExited(MouseEvent e){
        vision3D.translationMode = true;
        mouseX = e.getX();
        mouseY = e.getY();
    }
    public void mousePressed(MouseEvent e){

    }
    public void mouseReleased(MouseEvent e){

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        int newMouseX = mouseEvent.getX();
        int newMouseY = mouseEvent.getY();
        if(vision3D.cameraMode) {
			if(newMouseX != mouseX)
				this.vision3D.camera.yaw((float)( Math.toRadians( ((newMouseX - mouseX)>0)?-1:1 )));
			if(newMouseY != mouseY)
				this.vision3D.camera.pitch((float)( Math.toRadians( ((newMouseY - mouseY)>0)?-1:1 )));
        }
        mouseX = newMouseX;
        mouseY = newMouseY;
    }
}
